<?php

namespace Nyvky\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home_page")
     */
    public function indexAction()
    {
        return $this->render('NyvkyAppBundle:Default:index.html.twig');
    }

    /**
     * @Route("/secret", name="secret_page")
     */
    public function secretAction()
    {
        return $this->render('NyvkyAppBundle:Default:secret.html.twig');
    }
}
