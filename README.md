beercounter
===========

A Symfony project.

To install on local machine, you should have:

- Parallels;
- Vagrant;
- Vagrant plugin (vagrant plugin install vagrant-triggers)
- Git
- Composer (composer install need to be done)
if so, just run:

``vagrant up``

and have some coffee, because it takes some time ;)

and run ``composer install`` in the end.


